<?php


namespace App\Twig;


use App\Entity\Product;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ProductExtension extends AbstractExtension
{

    public function getFunctions()
    {
        return [
            new TwigFunction('product_view', [$this, 'getProductView'], [
                'is_safe' => ['html'],
                'needs_environment' => true
            ]),
        ];
    }

    public function getProductView(Environment $twig, Product $product)
    {
        return $twig->render('blocks/product_block.html.twig', [
            'product' => $product
        ]);
    }
}