<?php


namespace App\Twig;

use App\Service\Cart;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CartExtension extends AbstractExtension
{

    /**
     * @var Cart
     */
    private $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('cart', [$this, 'getCart']),
            new TwigFunction('cart_view', [$this, 'getCartView'], [
                'is_safe' => ['html'],
                'needs_environment' => true
            ]),
        ];
    }

    public function getCart(): Cart
    {
        return $this->cart;
    }

    public function getCartView(Environment $twig)
    {
        $viewProducts = $this->cart->getOrderProducts();
        return $twig->render('blocks/cart_block.html.twig', [
            'products' => $viewProducts,
            'totalPrices' => implode(' / ', $this->cart->getSignedTotalPrices()),
            'deliveryPrices' => $this->cart->getSignedDeliveryPrices()
        ]);
    }
}