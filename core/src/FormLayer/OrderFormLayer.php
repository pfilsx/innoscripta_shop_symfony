<?php

namespace App\FormLayer;

use App\Entity\AppUser;
use App\Service\Cart;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Pfilsx\FormLayer\Layer\EntityFormLayer;

use App\Entity\Order;
use function is_object;

/**
 * @method Order create(bool $force = false)
 * @method void load(Order $entity)
 * @property Order $entity
 */
class OrderFormLayer extends EntityFormLayer
{
    /**
     * @var Cart
     */
    private $cart;
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="255")
     */
    public $name;
    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^\+7\(\d{3}\)\d{3}\-\d{2}-\d{2}$/i")
     */
    public $phone;
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="500")
     */
    public $address;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(Cart $cart, TokenStorageInterface $storage)
    {
        $this->cart = $cart;
        $this->tokenStorage = $storage;
    }

    public function update()
    {
        if ($this->entity !== null) {
            $this->loadEntityFields();
            foreach ($this->cart->getOrderProducts() as $orderProduct) {
                $this->entity->addOrderProduct($orderProduct);
            }
            $this->entity->setUser($this->getUser());
            $this->entity->setTotalPrice(implode(' / ', $this->cart->getSignedTotalPrices()));
        }
    }

    private function getUser(): ?AppUser
    {
        if (($token = $this->tokenStorage->getToken()) === null) {
            return null;
        }

        if (!is_object($user = $token->getUser())) {
            return null;
        }
        /**
         * @var AppUser $user
         */
        return $user;
    }

    public static function getEntityClass(): string
    {
        return Order::class;
    }
}
