<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class ProductFixtures extends Fixture
{
    private $titles = [
        'Margherita',
        'Capricciosa',
        'Quattro Formaggi',
        'Bolognese',
        'Mexicana',
        'Peperoni',
        'Napolitana',
        'Hawaii',
    ];

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        for ($i = 0; $i < 8; $i++) {
            $product = new Product();
            $product
                ->setTitle($this->titles[$i])
                ->setDescription($faker->text(120))
                ->setPrice(random_int(300, 800));
            $manager->persist($product);
        }

        $manager->flush();
    }
}
