<?php


namespace App\Service;


use App\Entity\OrderProduct;
use App\Entity\Product;
use App\Helpers\CurrencyConverter;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Cart
{
    const DELIVERY_COST = 100;
    /**
     * @var array
     */
    private $storedProducts;
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var Product[]
     */
    private $productModels;
    /**
     * @var array
     */
    private $deliveryPrices;

    public function __construct(SessionInterface $session, ProductRepository $productRepository)
    {
        $this->session = $session;
        $this->productRepository = $productRepository;
        $this->storedProducts = $session->get('cart', []);
    }

    public function save()
    {
        $this->session->set('cart', $this->storedProducts);
        $this->session->save();
    }

    /**
     * Clear current cart and save it
     */
    public function clear()
    {
        $this->storedProducts = [];
        $this->save();
    }

    /**
     * Checks if current cart is empty
     * @return bool
     */
    public function isEmpty()
    {
        return empty($this->storedProducts);
    }

    /**
     * Returns count of products: total or by product id
     * @param null $id
     * @return int
     */
    public function count($id = null)
    {
        $totalCount = 0;
        foreach ($this->storedProducts as $productId => $productAmount) {
            if ($id === null || $productId == $id) {
                $totalCount += $productAmount;
            }
        }
        return $totalCount;
    }

    /**
     * Adds product in current cart and saves cart.
     * If product with same id already exists in cart increments its count
     * @param Product $product
     */
    // TODO required functionality to limit the quantity of products to improve business logic and avoid possible overflow
    public function addProduct(Product $product)
    {
        if (!array_key_exists($product->getId(), $this->storedProducts)) {
            $this->storedProducts[$product->getId()] = 1;
        } else {
            $this->storedProducts[$product->getId()]++;
        }
        $this->save();
    }

    /**
     * Removes product(fully or one instance) from current cart and saves cart.
     * If product with same id not in cart ignores it.
     * If after removing of the instance we have less than 1 instance of product in cart removes product fully.
     * @param Product $product
     * @param bool $fullRemove - forces fully remove of product from cart
     */
    public function removeProduct(Product $product, bool $fullRemove = false)
    {
        if (array_key_exists($product->getId(), $this->storedProducts)) {
            $fullRemove
                ? $this->storedProducts[$product->getId()] = 0
                : $this->storedProducts[$product->getId()]--;
            if ($this->storedProducts[$product->getId()] < 1) {
                unset($this->storedProducts[$product->getId()]);
            }
        }
        $this->save();
    }

    /**
     * Returns total prices of products in current cart in array format(eg ['us' => 35, 'eu' => 37]).
     * @return array
     */
    public function getTotalPrices()
    {
        $totalPrices = $this->getDeliveryPrices();
        $products = $this->getProductModels();
        foreach ($products as $product) {
            $count = $this->storedProducts[$product->getId()];
            $prices = $product->getPrices($count);
            foreach ($prices as $key => $price) {
                $totalPrices[$key] += $price;
            }
        }
        return $totalPrices;
    }

    /**
     * Returns total prices with their signs of products in current cart in array format(eg ['us' => '35$', 'eu' => '37€']).
     * @return array
     */
    public function getSignedTotalPrices()
    {
        $prices = $this->getTotalPrices();
        array_walk($prices, function (&$price, $currency) {
            $price = $price . CurrencyConverter::CURRENCY_SIGNS[$currency];
        });
        return $prices;
    }

    /**
     * Returns array of OrderProduct models for current cart.
     * @return array
     */
    public function getOrderProducts()
    {
        $result = [];
        $products = $this->getProductModels();
        foreach ($products as $product) {
            $count = $this->storedProducts[$product->getId()];
            $orderProduct = new OrderProduct();
            $orderProduct
                ->setCount($count)
                ->setProduct($product)
                ->setPrice(implode(' / ', $product->getSignedPrices($count)));
            $result[] = $orderProduct;
        }
        return $result;
    }

    public function getDeliveryPrices()
    {
        return $this->deliveryPrices ?? ($this->deliveryPrices = CurrencyConverter::convert(self::DELIVERY_COST));
    }
    public function getSignedDeliveryPrices()
    {
        $prices = $this->getDeliveryPrices();
        array_walk($prices, function (&$price, $currency) {
            $price = $price . CurrencyConverter::CURRENCY_SIGNS[$currency];
        });
        return $prices;
    }

    private function getProductModels()
    {
        return $this->productModels
            ?? ($this->productModels = $this->productRepository->findBy(['id' => array_keys($this->storedProducts)]));
    }


}