<?php

namespace App\Controller;

use App\Form\OrderType;
use App\FormLayer\OrderFormLayer;
use App\Repository\ProductRepository;
use App\Service\Cart;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MainController
 * @package App\Controller
 *
 * @Route("/")
 */
class MainController extends AbstractController
{
    /**
     * @param ProductRepository $repository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     * @Route("", name="app_index")
     */
    public function index(ProductRepository $repository, PaginatorInterface $paginator, Request $request): Response
    {
        $pagination = $paginator->paginate(
            $repository->createQueryBuilder('p'),
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('main/index.html.twig', [
            'products' => $pagination,
        ]);
    }

    /**
     * @param Request $request
     * @param Cart $cart
     * @param OrderFormLayer $formLayer
     * @return Response
     * @Route("/checkout", name="app_checkout", methods={"GET", "POST"})
     */
    public function checkoutForm(Request $request, Cart $cart, OrderFormLayer $formLayer): Response
    {
        if ($cart->isEmpty()) {
            return $this->redirectToRoute('app_index');
        }
        $form = $this->createForm(OrderType::class, $formLayer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // TODO handle db exceptions
            $this->getDoctrine()->getManager()->persist($formLayer->create());
            $this->getDoctrine()->getManager()->flush();
            $cart->clear();
            $this->addFlash('message', 'Order successfully created!');
            return $this->redirectToRoute('app_index');
        }
        return $this->render('main/checkout.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
