<?php


namespace App\Controller;


use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller
 *
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @return Response
     * @Route("/order-history", name="app_order_history")
     */
    public function orderHistory(Request $request, PaginatorInterface $paginator): Response
    {
        $pagination = $paginator->paginate(
            $this->getUser()->getOrders(),
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('order_history.html.twig', [
            'orders' => $pagination
        ]);
    }
}