<?php


namespace App\Controller;


use App\Repository\ProductRepository;
use App\Service\Cart;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CartController
 * @package App\Http\Controllers
 *
 * @Route("/cart")
 */
class CartController extends AbstractController
{
    /**
     * @return Response
     * @Route("", name="app_cart")
     */
    public function index(): Response
    {
        return $this->render('cart.html.twig');
    }

    /**
     * @return Response
     * @Route("/clear", name="app_cart_clear", condition="'%kernel.environment%' !== 'prod'")
     */
    public function clearCart(Cart $cart): Response
    {
        $cart->clear();
        return $this->redirectToRoute('app_index');
    }

    /**
     * @param Request $request
     * @param ProductRepository $repository
     * @param Cart $cart
     * @return Response
     * @Route("/add-product", name="app_cart_add_product", methods={"POST"})
     */
    public function addToCart(Request $request, ProductRepository $repository, Cart $cart): Response
    {
        if ($request->getRequestFormat('json')) {
            $product = $repository->find($request->get('product_id'));
            if ($product) {
                $cart->addProduct($product);
                return new JsonResponse([
                    'success' => true,
                    'result' => [
                        'products_count' => $cart->count()
                    ]
                ]);
            }
            return new JsonResponse([
                'success' => false,
                'result' => [
                    'message' => 'Selected product not found in our database'
                ]
            ]);
        }
        return $this->redirectToRoute('app_index');
    }

    /**
     * @param Request $request
     * @param ProductRepository $repository
     * @param Cart $cart
     * @return Response
     * @Route("/remove-product", name="app_cart_remove_product", methods={"POST"})
     */
    public function removeFromCart(Request $request, ProductRepository $repository, Cart $cart): Response
    {
        if ($request->getRequestFormat('json')) {
            $product = $repository->find($request->get('product_id'));
            if ($product) {
                $cart->removeProduct($product);
            }
            return new JsonResponse([
                'success' => true,
                'result' => [
                    'products_count' => $cart->count()
                ]
            ]);
        }
        return $this->redirectToRoute('app_index');
    }
}
