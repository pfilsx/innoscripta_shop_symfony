<?php

namespace Tests\Feature;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ResponsesTest extends WebTestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $client = static::createClient();
        $client->request('GET', '/');
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertSelectorExists('.product-card .btn-to-cart');
    }

    public function testCart()
    {
        $client = static::createClient();
        $client->request('GET', '/cart');
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertSelectorTextContains('p', 'Your Order Cart is empty.');
    }

    // expected redirect if cart is empty
    public function testCheckout()
    {
        $client = static::createClient();
        $client->request('GET', '/checkout');
        $response = $client->getResponse();
        $this->assertTrue($response->isRedirect("/"));

        $client->request('POST', '/checkout', ['name' => 'test', 'phone' => '+7(911)111-11-11', 'address' => 'test address']);
        $response = $client->getResponse();
        $this->assertTrue($response->isRedirect("/"));
    }

    public function testHistory()
    {
        $client = static::createClient();
        $client->request('GET', '/user/order-history');
        $response = $client->getResponse();
        $this->assertTrue($response->isRedirect("/login"));

        $form =  $client->followRedirect()->filter('form')->form();
        $form->setValues(['username' => 'test', 'password' => 'test']);
        $client->submit($form);
        $response = $client->getResponse();
        $this->assertTrue($response->isRedirect("/"));
        $client->request('GET', '/user/order-history');
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertSelectorTextContains('h1', 'Orders History');
    }

    public function testAddToCart()
    {
        $client = static::createClient();
        $client->request('GET', '/');
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $product = $client->getCrawler()->filter('.btn-to-cart');
        $client->xmlHttpRequest('POST', '/cart/add-product', ['product_id' => $product->attr('data-id')], [], ['Accept' => 'application/json']);
        $this->assertJson(json_encode([
            'success' => true,
            'result' => [
                'products_count' => 1
            ]
        ]), $client->getResponse()->getContent());

        $client->request('GET', '/');
        $this->assertEquals('1', $client->getCrawler()->filter('.cart-item-counter')->text());

        $client->xmlHttpRequest('POST', '/cart/add-product', [], [], ['Accept' => 'application/json']);
        $this->assertJson(json_encode([
            'success' => false,
            'result' => [
                'message' => 'Selected product not found in our database.'
            ]
        ]), $client->getResponse()->getContent());
        $client->request('GET', '/cart/clear');
    }

    public function testRemoveFromCart()
    {
        $client = static::createClient();
        $client->request('GET', '/');
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $product = $client->getCrawler()->filter('.btn-to-cart');
        $client->xmlHttpRequest('POST', '/cart/add-product', ['product_id' => $product->attr('data-id')], [], ['Accept' => 'application/json']);
        $this->assertJson(json_encode([
            'success' => true,
            'result' => [
                'products_count' => 1
            ]
        ]), $client->getResponse()->getContent());

        $client->request('GET', '/');
        $this->assertEquals('1', $client->getCrawler()->filter('.cart-item-counter')->text());

        $client->xmlHttpRequest('POST', '/cart/remove-product', ['product_id' => $product->attr('data-id')], [], ['Accept' => 'application/json']);
        $this->assertJson(json_encode([
            'success' => true,
            'result' => [
                'products_count' => 0
            ]
        ]), $client->getResponse()->getContent());

        $client->request('GET', '/');
        $this->assertEquals('0', $client->getCrawler()->filter('.cart-item-counter')->text());

        // checks what we don't have -1 product count in cart after this
        $client->xmlHttpRequest('POST', '/cart/remove-product', ['product_id' => $product->attr('data-id')], [], ['Accept' => 'application/json']);
        $this->assertJson(json_encode([
            'success' => true,
            'result' => [
                'products_count' => 0
            ]
        ]), $client->getResponse()->getContent());

        $client->request('GET', '/');
        $this->assertEquals('0', $client->getCrawler()->filter('.cart-item-counter')->text());
    }
}
