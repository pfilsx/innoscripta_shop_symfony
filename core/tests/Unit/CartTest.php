<?php


namespace Tests\Unit;




use App\Service\Cart;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

use Tests\Entity\Product;

class CartTest extends KernelTestCase
{
    /**
     * @var \App\Service\Cart|object|null
     */
    private $cart;

    protected function setUp(): void
    {
        $kernel = static::createKernel();
        $kernel->boot();
        $container = $kernel->getContainer();
        $this->cart = $container->get('App\Service\Cart');
    }

    public function testSingleton()
    {
        $this->assertInstanceOf(Cart::class, $this->cart);
    }

    /**
     * @param array $products
     *
     * @dataProvider productsProvider
     */
    public function testAddProduct(array $products)
    {
        $cart = $this->cart;
        $this->assertTrue($cart->isEmpty());
        $productsCount = [];
        foreach ($products as $product) {
            $cart->addProduct($product);
            array_key_exists($product->getId(), $productsCount)
                ? $productsCount[$product->getId()]++
                : $productsCount[$product->getId()] = 1;
        }
        $this->assertFalse($cart->isEmpty());
        $this->assertEquals(count($products), $cart->count());
        foreach ($productsCount as $id => $count) {
            $this->assertEquals($count, $cart->count($id));
        }
        $cart->clear();
        $this->assertTrue($cart->isEmpty());
    }

    /**
     * @param array $products
     *
     * @dataProvider productsProvider
     */
    public function testRemoveProduct(array $products)
    {
        $cart = $this->cart;
        $this->assertTrue($cart->isEmpty());
        $productsCount = [];
        foreach ($products as $product) {
            $cart->addProduct($product);
            array_key_exists($product->getId(), $productsCount)
                ? $productsCount[$product->getId()]++
                : $productsCount[$product->getId()] = 1;
        }
        $removeProduct = $products[0];
        $cart->removeProduct($removeProduct);
        $this->assertEquals(--$productsCount[$removeProduct->getId()], $cart->count($removeProduct->getId()));
        $cart->removeProduct($removeProduct, true);
        $this->assertEquals(0, $cart->count($removeProduct->getId()));
        $cart->clear();
        $this->assertTrue($cart->isEmpty());
    }

    public function productsProvider()
    {
        yield [
            [
                new Product([
                    'id' => 1,
                    'title' => 'Test Product',
                    'description' => 'Test product',
                    'price' => 100,
                    'imageUrl' => 'test.png'
                ])
            ]
        ];
        yield [
            [
                new Product([
                    'id' => 1,
                    'title' => 'Test Product',
                    'description' => 'Test product',
                    'price' => 100,
                    'imageUrl' => 'test.png'
                ]),
                new Product([
                    'id' => 1,
                    'title' => 'Test Product',
                    'description' => 'Test product',
                    'price' => 100,
                    'imageUrl' => 'test.png'
                ])
            ]
        ];
        yield [
            [
                new Product([
                    'id' => 1,
                    'title' => 'Test Product',
                    'description' => 'Test product',
                    'price' => 100,
                    'imageUrl' => 'test.png'
                ]),
                new Product([
                    'id' => 1,
                    'title' => 'Test Product',
                    'description' => 'Test product',
                    'price' => 100,
                    'imageUrl' => 'test.png'
                ]),
                new Product([
                    'id' => 2,
                    'title' => 'Test Product',
                    'description' => 'Test product',
                    'price' => 100,
                    'imageUrl' => 'test.png'
                ])
            ]
        ];
    }
}
