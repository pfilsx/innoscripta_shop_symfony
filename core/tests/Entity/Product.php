<?php


namespace Tests\Entity;


class Product extends \App\Entity\Product
{
    public function __construct(array $params)
    {
        foreach ($params as $attr => $value) {
            if (property_exists($this, $attr)) {
                $this->$attr = $value;
            }
        }
        parent::__construct();
    }
}