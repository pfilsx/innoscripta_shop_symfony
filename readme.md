## Build

1. Fill `APP_SECRET` and `DATABASE_URL` in `.env` file | and `DATABASE_URL` in `phpunit.xml.dist` for testing
2. `composer install` (`--no-dev --optimize-autoload` - for prod) in core directory
3. `yarn install` in core directory
4. Fox `setPublicPath` in `webpack.config.js` for your environment
5. `yarn encore dev` (or ` prod`)